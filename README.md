Laravel Check VAT status
================

[Sprawdzenie statusu podmiotu w VAT](https://ppuslugi.mf.gov.pl/_/) integration for Laravel.

General System Requirements
-------------
- [PHP >7.1.0](http://php.net/)
- [Laravel ~5.6.*](https://github.com/laravel/framework)
- [Laravel SoapClient >1.0.0](https://bitbucket.org/rudashi/laravelsoap)

Quick Installation
-------------
If needed use composer to grab the library

```
$ composer require rudashi/nip-vat
```

Usage
-------------

###Dependency Injection

```php
public function __construct(\Rudashi\NipVat\NipVat $nipVat)
{
    $this->nipApi = $nipVat;
}
```

###Facade

```php
NipVatFacade::checkNip($nip);
```

or directly

```php
$nipApi = NipVatFacade::instance();
```

###Examples

```php
$this->nipApi->checkNip($nip);
```

Authors
-------------

* **Borys Zmuda** - Lead designer - [GoldenLine](http://www.goldenline.pl/borys-zmuda/)