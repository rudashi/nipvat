<?php

namespace Rudashi\NipVat;

use Rudashi\NipVat\Events\LogStatus;
use Rudashi\NipVat\Response\NipResponse;
use Rudashi\NipVat\Traits\CanLogToDatabase;
use Rudashi\LaravelSoap\Client\Soap;
use Rudashi\LaravelSoap\Client\SoapService;
use Rudashi\LaravelSoap\Client\SoapTrait;
use Rudashi\NipVat\Exceptions\InvalidNipException;

class NipVat
{

    use SoapTrait,
        CanLogToDatabase;

    public const SERVICE = 'NIP';

    /**
     * @var Soap
     */
    private $soap;

    public function __construct()
    {
        $this->build();
    }

    public function instance() : NipVat
    {
        return $this;
    }

    protected function build() : Soap
    {
        $this->soap = new Soap();
        $this->soap->addService(self::SERVICE, function(SoapService $service) {
            $service->setWsdl('https://sprawdz-status-vat.mf.gov.pl/?wsdl')
                ->setTrace(true)
                ->setSoapVersion(SOAP_1_1)
            ;
        });

        return $this->soap;
    }

    public function checkNip(?string $nip) : NipResponse
    {
        $this->verifyNip($nip);

        $response = new NipResponse($this->call('SprawdzNIP', [
            'NIP' => $nip
        ]));

        event(new LogStatus($nip, $response, $this));

        return $response;
    }

    public function call(string $function, array $data = [])
    {
        return $this->soap->call(self::SERVICE, $function, $data);
    }

    public function verifyNip(?string $nip) : bool
    {
        if (\strlen($nip) !== 10) {
            throw new InvalidNipException('Invalid Tax Identification Number.');
        }
        return true;
    }

    public function isMigration() : bool
    {
        return \Illuminate\Support\Facades\DB::table('migrations')->where('migration', '2018_06_20_080000_create_vat_status')->exists();
    }
}