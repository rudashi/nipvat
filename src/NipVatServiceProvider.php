<?php

namespace Rudashi\NipVat;

use Illuminate\Support\ServiceProvider;

class NipVatServiceProvider extends ServiceProvider
{

    public function getNamespace() : string
    {
        return 'nip-vat';
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() : void
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');

        \Illuminate\Support\Facades\Event::listen(
            \Rudashi\NipVat\Events\LogStatus::class,
            \Rudashi\NipVat\Listeners\LogStatusToDatabase::class
        );

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() : void
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());
    }
}