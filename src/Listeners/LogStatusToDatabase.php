<?php

namespace Rudashi\NipVat\Listeners;

use Rudashi\NipVat\Events\LogStatus;

class LogStatusToDatabase
{

    public function handle(LogStatus $event) : void
    {
        if ($event->canLog()) {

            $event->getApi()->log($event->getNip(), $event->response);
        }
    }

    public function failed(LogStatus $event, $exception) : void
    {
        //
    }
}