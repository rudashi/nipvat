<?php

namespace Rudashi\NipVat\Response;

use Rudashi\NipVat\Exceptions\InternalServiceFaultException;

class NipResponse
{

    public $code;
    public $message;

    public function __construct($response)
    {
        $this->setProperties($response);
    }

    public function setProperties($response) : self
    {
        if ($response instanceof \SoapFault) {
            throw new InternalServiceFaultException($response->getMessage());
        }

        $this->code     = $response->Kod;
        $this->message  = $response->Komunikat;

        return $this;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function getMessage() : string
    {
        return $this->message;
    }

}