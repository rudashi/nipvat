<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVatStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        try{
            Schema::create(config('nip-vat.table'), function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->string('code');
                $table->string('vat_number');
                $table->text('request_header')->nullable();
                $table->text('request_text')->nullable();
                $table->text('response_header')->nullable();
                $table->text('response_text')->nullable();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists(config('nip-vat.table'));
    }
}
