<?php

Route::group(['prefix' => 'api'], static function() {

    Route::group(['prefix' => 'vat'], static function(){

        Route::get('status/{nip?}', static function($nip = null){
            return new \Totem\SamCore\App\Resources\Api(
                (new \Rudashi\NipVat\Api())->checkNip($nip)
            );
        })->name('api.vat.status')->where('nip', '[0-9]+');
    });

});