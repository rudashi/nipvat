<?php

namespace Rudashi\NipVat;

use Totem\SamCore\App\Traits\HasApi;

class Api
{

    use HasApi;

    private $nip;

    public function __construct()
    {
        $this->nip = new NipVat();
    }

    public function checkNip(?string $nip)
    {
        try {
            return $this->success($this->nip->checkNip($nip));
        } catch (\Exception $exception) {
            return $this->error(400, $exception->getMessage());
        }
    }

}