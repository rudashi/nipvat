<?php

namespace Rudashi\NipVat\Events;

use Rudashi\NipVat\NipVat;
use Rudashi\NipVat\Response\NipResponse;

class LogStatus
{

    protected $can_log;
    protected $api;
    protected $nip;
    public $response;

    public function __construct(string $nip, NipResponse $response, NipVat $nipVat)
    {
        $this->can_log  = config('nip-vat.logs', true);
        $this->response = $response;
        $this->api      = $nipVat;
        $this->nip      = $nip;
    }

    public function canLog() : bool
    {
        if ($this->api->isMigration()) {
            return $this->can_log;
        }
        return false;
    }

    public function getApi() : NipVat
    {
        return $this->api;
    }

    public function getNip() : string
    {
        return $this->nip;
    }
}