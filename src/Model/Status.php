<?php

namespace Rudashi\NipVat\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string code
 * @property string vat_number
 * @property string request_header
 * @property string request_text
 * @property string response_header
 * @property string response_text
 */
class Status extends Model
{

    public function __construct(array $attributes = [])
    {
        $this->setTable(config('nip-vat.table'));
        $this->fillable([
            'code',
            'vat_number',
            'request_header',
            'request_text',
            'response_header',
            'response_text',
        ]);

        parent::__construct($attributes);
    }

}