<?php

namespace Rudashi\NipVat;

use Illuminate\Support\Facades\Facade;

class NipVatFacade extends Facade
{

    protected static function getFacadeAccessor() : string
    {
        return NipVat::class;
    }
}