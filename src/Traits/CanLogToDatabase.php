<?php

namespace Rudashi\NipVat\Traits;

use Rudashi\NipVat\Model\Status;
use Rudashi\NipVat\Response\NipResponse;

trait CanLogToDatabase
{

    public function log(string $nip, NipResponse $response) : Status
    {
        $status = new Status();

        $status->code               = $response->getCode();
        $status->vat_number         = $nip;
        $status->request_header     = $this->getLastRequestHeaders();
        $status->request_text       = $this->getLastRequest();
        $status->response_header    = $this->getLastResponseHeaders();
        $status->response_text      = $this->getLastResponse();

        $status->save();

        return $status;
    }
}