<?php

namespace NipVat\Tests;

use Rudashi\NipVat\NipVat;
use Rudashi\NipVat\Response\NipResponse;
use Rudashi\NipVat\Exceptions\InvalidNipException;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class NipVatTest extends TestCase
{

    use CreatesApplication;

    private $nip  = '5561007611';
    private $nipN = '5561350622';
    private $nipZ = '6772410741';
    private $nipI = 'abcdefghij';

    /**
     * @var NipVat
     */
    protected $api;

    public function setUp()
    {
        $this->api = new NipVat();
        $this->createApplication();
    }

    public function testVerifyInvalidNip() : void
    {
        $this->expectException(InvalidNipException::class);

        $this->api->verifyNip('abcdefghijklmnop');
    }

    public function testVerifyNip() : void
    {
        $this->assertTrue($this->api->verifyNip($this->nip));
    }

    public function testCheckInvalidNip() : void
    {
        $this->expectException(InvalidNipException::class);

        $this->api->checkNip('abcdefghijklmnop');
    }

    public function testCheckNip() : void
    {
        $this->assertInstanceOf(NipResponse::class, $this->api->checkNip($this->nip));
    }

    public function testResponseCodeCorrect() : void
    {
        $this->assertSame('C', $this->api->checkNip($this->nip)->getCode());
    }

    public function testResponseCodeInactive() : void
    {
        $this->assertSame('N', $this->api->checkNip($this->nipN)->getCode());
    }

    public function testResponseCodeReleased() : void
    {
        $this->assertSame('Z', $this->api->checkNip($this->nipZ)->getCode());
    }

    public function testResponseCodeInvalid() : void
    {
        $this->assertSame('I', $this->api->checkNip($this->nipI)->getCode());
    }

}