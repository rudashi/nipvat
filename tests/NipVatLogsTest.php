<?php

namespace NipVat\Tests;

use Rudashi\NipVat\Events\LogStatus;
use Rudashi\NipVat\NipVat;
use Tests\CreatesApplication;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Testing\TestCase;

class NipVatLogsTest extends TestCase
{

    use CreatesApplication;

    /**
     * @var NipVat
     */
    protected $api;

    public function setUp() : void
    {
        parent::setUp();

        $this->api = new NipVat();
    }

    public function testIsMigrationFalse() : void
    {
        $this->assertFalse($this->api->isMigration());
    }

    public function testIsMigration() : void
    {
        $this->migrate();

        $this->assertTrue($this->api->isMigration());

        $this->migrate(true);
    }

    public function testCheckVatStatus() : void
    {
        $this->migrate();

        Event::fake(LogStatus::class);

        Event::fire(LogStatus::class);

        Event::assertDispatched(LogStatus::class, 1);

        $this->migrate(true);

    }

    protected function migrate(bool $rollback = false) : void
    {
        \Illuminate\Support\Facades\Artisan::call('migrate'.($rollback ? ':rollback' : ''), [
            '--env'  => 'local',
        ]);
    }
}